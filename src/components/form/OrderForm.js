import "../form/OrderForm.scss";
import { useSelector } from "react-redux";
import { useFormik } from "formik";
import * as Yup from "yup";

function OrderForm() {
  const products = useSelector((state) => state.products);

  const formik = useFormik({
    initialValues: {
      name: "",
      surname: "",
      age: "",
      phone: "",
      adress: "",
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .max(15, "Поле повинне містити 15 знаків або менше")
        .required("Поле обов'язкове!"),
      surname: Yup.string()
        .max(20, "Поле повинне містити 20 знаків або менше")
        .required("Поле обов'язкове!"),
      age: Yup.number()
        .typeError("Це поле приймає лише числа на ввід")
        .positive("Значення поля має бути більшим за 0")
        .required("Поле обов'язкове!"),
      phone: Yup.string()
        .matches(/^\+380\d{9}$/, "Невірний номер, спробуйте ще раз!")
        .required("Поле обов'язкове!"),
      adress: Yup.string().required("Поле обов'язкове!"),
    }),
    onSubmit: (values) => {
      // console.log("submit", values);
    },
  });

  const handleCheckout = () => {
    const purchasedProducts = products
      .filter((product) => product.isAddedToCart)
      .map((product) => product.name);
    console.log("Придбані товари:", purchasedProducts);

    console.log("Дані з форми:", formik.values);
  };

  if (!products.filter((product) => product.isAddedToCart).length) {
    return;
  }

  return (
    <form onSubmit={formik.handleSubmit} className="orderForm">
      <h1 className="h1"> Оформлення замовлення</h1>
      <h4>Особисті дані</h4>
      <div className="orderFormWrapper">
        <label className="label">Ім'я</label>
        <input
          type="text"
          name="name"
          className="userName"
          placeholder="Введіть ім'я"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
        />
        {formik.touched.name && formik.errors.name ? (
          <div className="errorMessage">{formik.errors.name}</div>
        ) : null}

        <label className="label">Прізвище</label>
        <input
          type="text"
          name="surname"
          className="userName"
          placeholder="Введіть прізвище"
          onChange={formik.handleChange}
          value={formik.values.surname}
          onBlur={formik.handleBlur}
        />
        {formik.touched.surname && formik.errors.surname ? (
          <div className="errorMessage">{formik.errors.surname}</div>
        ) : null}

        <label className="label">Ваш вік</label>
        <input
          type="text"
          name="age"
          className="userAge"
          placeholder="Введіть вік"
          onChange={formik.handleChange}
          value={formik.values.age}
          onBlur={formik.handleBlur}
        />
        {formik.touched.age && formik.errors.age ? (
          <div className="errorMessage">{formik.errors.age}</div>
        ) : null}

        <label className="label">Телефон</label>
        <input
          type="tel"
          name="phone"
          placeholder="+ 38 (__) ___ __ __"
          className="phoneNumber"
          onChange={formik.handleChange}
          value={formik.values.phone}
          onBlur={formik.handleBlur}
        />
        {formik.touched.phone && formik.errors.phone ? (
          <div className="errorMessage">{formik.errors.phone}</div>
        ) : null}
      </div>

      <h4>Доставка</h4>

      <div className="orderFormWrapper">
        <label className="label">Адреса доставки</label>
        <input
          type="text"
          name="adress"
          className="adressToOrder"
          placeholder="Введіть адресу: місто, вулиця, номер будинку"
          onChange={formik.handleChange}
          value={formik.values.adress}
          onBlur={formik.handleBlur}
        />
        {formik.touched.adress && formik.errors.adress ? (
          <div className="errorMessage">{formik.errors.adress}</div>
        ) : null}
      </div>
      <button
        type="submit"
        className="checkoutButton"
        onClick={() => {
          handleCheckout();
        }}
      >
        Checkout
      </button>
    </form>
  );
}
export default OrderForm;
