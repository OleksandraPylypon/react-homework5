import React, { useState } from "react";
import "./Products.scss";
import { useLocation } from "react-router-dom";
import Dialog from "../dialog/Dialog";
import "../dialog/Dialog.module.scss";
import { useDispatch } from "react-redux";
import { toggleCart, toggleFavorites } from "../../rootReducers";

function ProductsCard(props) {
  const location = useLocation();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const dispatch = useDispatch();

  const closeDialog = () => {
    setIsDialogOpen(false);
  };

  const openDialog = () => {
    setIsDialogOpen(true);
  };

  const confirmDialog = () => {
    dispatch(toggleCart(props.name));
    closeDialog();
  };

  const openModal = () => {
    dispatch(toggleCart(props.name));
  };

  const toggleFavorite = () => {
    dispatch(toggleFavorites(props.name));
  };

  return (
    <div className="products-card">
      <img
        className={`icon-star ${props.isFavorite ? "favorite" : ""}`}
        src={
          props.isFavorite ? "/images/star-yellow.svg" : "/images/starblack.svg"
        }
        alt="star-icon"
        onClick={toggleFavorite}
      />

      {location.pathname === "/cart" && (
        <img
          className="icon-close"
          src="/images/close.svg"
          alt="icon"
          onClick={openDialog}
        />
      )}
      <img className="products-image" src={props.imagePath} alt="" />
      <p className="products-articleNumber">Артикул: {props.articleNumber}</p>
      <p className="products-name">{props.name}</p>
      <p className="products-price">{props.price}</p>
      <p className="products-color">Color: {props.color}</p>

      {location.pathname !== "/cart" && (
        <button
          className={`products-button ${props.isAddedToCart && "active"}`}
          onClick={openModal}
        >
          {props.isAddedToCart ? "Added" : "Add to cart"}
        </button>
      )}

      {isDialogOpen && (
        <Dialog
          header="Do you want to delete this product from the cart?"
          closeButton={true}
          text="Are you sure you want to delete it?"
          actions={
            <>
              <button className="custom-button" onClick={confirmDialog}>
                OK
              </button>
              <button className="custom-button" onClick={closeDialog}>
                Cancel
              </button>
            </>
          }
          onConfirm={confirmDialog}
          onClose={closeDialog}
        />
      )}
    </div>
  );
}

export default ProductsCard;
