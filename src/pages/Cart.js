import ProductsList from "../components/products/ProductsList";
import OrderForm from "../components/form/OrderForm";

function Cart() {
  return (
    <>
      <ProductsList />
      <OrderForm />
    </>
  );
}
export default Cart;
