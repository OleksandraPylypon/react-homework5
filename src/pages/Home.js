import ProductsList from "../components/products/ProductsList";

function Home() {
  return <ProductsList />;
}
export default Home;
